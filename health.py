# -*- coding: utf-8 -*-
# This file is part of health_cepsap_icd10 module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from sql import Null

from trytond import backend
from trytond.model import ModelView, ModelSQL, ModelSingleton,\
    ValueMixin, fields

from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool
from trytond.modules.company.model import(
    CompanyMultiValueMixin, CompanyValueMixin)
#from trytond.tools.multivalue import migrate_property

__all__ = ['PathologyCategoryConfiguration',
    'PathologyCategoryConfigurationSetup',
    'PathologyCategory', 'Pathology',
    'PathologyGroup', 'DiseaseMembers', 'PrescriptionLine',
    'PatientEvaluation']


class PathologyCategoryConfiguration(ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Disease Category Configuration'
    __name__ = 'gnuhealth.pathology.category.configuration'

    icd10_category = fields.MultiValue(fields.Many2One(
        'gnuhealth.pathology.category', 'ICD-10 Category',
        domain=[('parent', '=', None)]))
    cepsap_category = fields.MultiValue(fields.Many2One(
        'gnuhealth.pathology.category', 'CEPS-AP Category',
        domain=[('parent', '=', None)]))
    
    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in {'icd10_category','cepsap_category'}:
            return pool.get('gnuhealth.pathology.category.configuration.setup')
        return super(PathologyCategoryConfiguration, cls).multivalue_model(field)


class PathologyCategoryConfigurationSetup(ModelSQL, CompanyValueMixin):
    'Disease Category Configuration Setup'
    __name__ = 'gnuhealth.pathology.category.configuration.setup'

    icd10_category = fields.Many2One(
        'gnuhealth.pathology.category', 'ICD-10 Category',
        domain=[('parent', '=', None)])
    cepsap_category = fields.Many2One(
        'gnuhealth.pathology.category', 'CEPS-AP Category',
        domain=[('parent', '=', None)])

    
class PathologyCategory(metaclass=PoolMeta):
    __name__ = 'gnuhealth.pathology.category'

    full_name = fields.Function(fields.Char('Category Name'),
        'get_rec_name', searcher='search_rec_name')

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        category = cls.__table__()

        super(PathologyCategory, cls).__register__(module_name)

        icd10_name = 'ICD-10'
        cepsap_name = 'CEPS-AP'

        cursor.execute(*category.select(category.id,
            where=(category.name == icd10_name)))
        if not cursor.fetchone():
            cursor.execute(*category.insert([category.name], [[icd10_name]]))
            cursor.execute(*category.select(category.id,
                where=(category.name == icd10_name)))
            icd10_id = cursor.fetchone()
            cursor.execute(*category.update([category.parent], [icd10_id],
                where=((category.parent == Null) &
                    (category.name != icd10_name) &
                    (category.name != cepsap_name))))


class Pathology(metaclass=PoolMeta):
    __name__ = 'gnuhealth.pathology'

    main_category = fields.Function(fields.Selection([
        (None, ''),
        ('icd10', 'ICD-10'),
        ('cepsap', 'CEPS-AP'),
        ], 'Classification'),
        'get_main_category', searcher='search_main_category')

    @classmethod
    def get_main_category(cls, pathologies, name):
        pool = Pool()
        Config = pool.get('gnuhealth.pathology.category.configuration')
        config = Config(1)

        result = {}
        for p in pathologies:
            result[p.id] = None
            if p.category:
                main_category = cls._get_main_category(p.category)
                if main_category == config.icd10_category:
                    result[p.id] = 'icd10'
                elif main_category == config.cepsap_category:
                    result[p.id] = 'cepsap'
        return result

    @staticmethod
    def _get_main_category(category):
        if category.parent:
            return Pathology._get_main_category(category.parent)
        return category

    @classmethod
    def search_main_category(cls, name, clause):
        pool = Pool()
        Config = pool.get('gnuhealth.pathology.category.configuration')
        PathologyCategory = pool.get('gnuhealth.pathology.category')

        if clause[1] not in ('=', '!='):
            return []

        config = Config(1)
        main_category = clause[2]
        if main_category == 'icd10':
            main_category_id = (config.icd10_category and
                config.icd10_category.id or None)
        elif main_category == 'cepsap':
            main_category_id = (config.cepsap_category and
                config.cepsap_category.id or None)
        else:
            return []

        categories = PathologyCategory.search([
            ('parent', 'child_of', [main_category_id]),
            ])
        res = [c.id for c in categories]

        if clause[1] == '=':
            return [('category', 'in', res)]
        elif clause[1] == '!=':
            return [('category', 'not in', res)]
        return []


class PathologyGroup(metaclass=PoolMeta):
    __name__ = 'gnuhealth.pathology.group'

    main_category = fields.Selection([
        (None, ''),
        ('icd10', 'ICD-10'),
        ('cepsap', 'CEPS-AP'),
        ], 'Classification',
        states={'readonly': Bool(Eval('members'))},
        depends=['members'])


class DiseaseMembers(metaclass=PoolMeta):
    __name__ = 'gnuhealth.disease_group.members'

    @classmethod
    def __setup__(cls):
        super(DiseaseMembers, cls).__setup__()
        cls.disease_group.domain = [
            ('main_category', '=',
                Eval('_parent_name', {}).get('main_category'))]


class PrescriptionLine(metaclass=PoolMeta):
    __name__ = 'gnuhealth.prescription.line'

    indication_domain = fields.Selection([
        (None, ''),
        ('icd10', 'ICD-10'),
        ('cepsap', 'CEPS-AP'),
        ], 'Classification',
        states={'readonly': Bool(Eval('indication'))},
        depends=['indication'])

    @classmethod
    def __setup__(cls):
        super(PrescriptionLine, cls).__setup__()
        cls.indication.domain = ['OR',
            ('id', '=', Eval('indication')),
            ('main_category', '=', Eval('indication_domain'))]
        if 'indication_domain' not in cls.indication.depends:
            cls.indication.depends.append('indication_domain')


class PatientEvaluation(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.evaluation'

    diagnosis_domain = fields.Selection([
        (None, ''),
        ('icd10', 'ICD-10'),
        ('cepsap', 'CEPS-AP'),
        ], 'Classification',
        states={'readonly': Bool(Eval('diagnosis'))},
        depends=['diagnosis'])

    @classmethod
    def __setup__(cls):
        super(PatientEvaluation, cls).__setup__()
        cls.diagnosis.domain = ['OR',
            ('id', '=', Eval('diagnosis')),
            ('main_category', '=', Eval('diagnosis_domain'))]
        if 'diagnosis_domain' not in cls.diagnosis.depends:
            cls.diagnosis.depends.append('diagnosis_domain')
